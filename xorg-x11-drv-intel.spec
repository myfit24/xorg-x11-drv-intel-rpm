%define moduledir %(pkg-config xorg-server --variable=moduledir)
%define driverdir %{moduledir}/drivers
%define gitcommit b74b67f0f321875492968f7097b9d6e82a66d7df

Name:           xorg-x11-drv-intel
Version:        2.99.917_b74b67f0
Release:        1%{?dist}
Summary:        Xorg X11 Intel video driver for EL9, amd64 only, TearFree enabled by default
License:        MIT
URL:            https://gitlab.freedesktop.org/xorg/driver/xf86-video-intel
Source0:        https://gitlab.freedesktop.org/xorg/driver/xf86-video-intel/-/archive/%{gitcommit}/amd64.tar.gz

BuildRequires:  autoconf automake xorg-x11-util-macros xorg-x11-server-devel libdrm-devel xorg-x11-server-source 
Requires:       Xorg polkit


%description
X.Org X11 Intel video driver.


%prep
%setup -q -n xf86-video-intel-%{gitcommit}


%build

# https://src.fedoraproject.org/rpms/xorg-x11-drv-intel/blob/rawhide/f/xorg-x11-drv-intel.spec#_75
# It seems the driver won't build wit LTO enabled.
# Also see https://fedoraproject.org/wiki/LTOByDefault#Documentation
%global _lto_cflags %nil

autoreconf --force --install
%configure --enable-kms-only --enable-tear-free --with-default-dri=3 --disable-xvmc
%make_build


%install
%make_install

# Remove useless file to avoid RPM build error
rm %{buildroot}/%{driverdir}/intel_drv.la


%files
%doc COPYING
%{driverdir}/intel_drv.so
%{_mandir}/man4/intel.4.gz
%{_libexecdir}/xf86-video-intel-backlight-helper
%{_datadir}/polkit-1/actions/org.x.xf86-video-intel.backlight-helper.policy



%changelog
* Fri Sep 01 2023 avh
- initial specfile creation

